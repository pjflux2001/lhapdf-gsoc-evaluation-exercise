ARG BUILDER_IMAGE=atlasamglab/stats-base:root6.24.00
FROM ${BUILDER_IMAGE} as builder

USER root
WORKDIR /usr/local

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN apt-get -qq -y update && \
    apt-get -qq -y --no-install-recommends install \
      gcc \
      g++ \
      gfortran \
      make \
      cmake \
      libboost-all-dev \
      vim \
      zlibc \
      zlib1g-dev \
      libbz2-dev \
      rsync \
      git \
      bash-completion \
      libgsl-dev \
      wget && \
    pip install argparse && \
    apt-get -y autoclean && \
    apt-get -y autoremove && \
    rm -rf /var/lib/apt-get/lists/*
