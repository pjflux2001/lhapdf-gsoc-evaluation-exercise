import lhapdf
import numpy as np
import argparse
import sys
import pickle
import os.path

isStable = True

# Create an argument parser to accept a value from the command line
parser = argparse.ArgumentParser()
parser.add_argument('-p', '--pdf_set_name', type=str, help='pdf dataset name that is to be tested')
args = parser.parse_args()

# Use the value provided on the command line or fallback to a default value
if args.pdf_set_name is not None:
    pdf_set_name = args.pdf_set_name
else:
    pdf_set_name = 'NNPDF40_nnlo_as_01180' # Default value

# Load the desired PDF set
pdf = lhapdf.getPDFSet(pdf_set_name).mkPDF(0)
pdf_info = pdf.info()

# Check that the PDF set is available and contains the necessary information
checkAllRequiredKeys=True
requiredKeys = ['Authors','DataVersion','ErrorType','FlavorScheme','Flavors','Format','MBottom','MCharm','MDown','MStrange','MTop','MUp','MZ','NumFlavors','NumMembers','OrderQCD','Particle','QMax','QMin','Reference','SetDesc','SetIndex','XMax','XMin']
for requiredKey in requiredKeys:
	checkAllRequiredKeys = checkAllRequiredKeys & pdf_info.has_key(requiredKey)
if not checkAllRequiredKeys:
    print("Not all required keys are present")
    isStable = False

# Getting min and max of real-valued parameters
x_min=pdf_info.get_entry('XMin')
x_max=pdf_info.get_entry('XMax')
q_min=pdf_info.get_entry('QMin')
q_max=pdf_info.get_entry('QMax')

# Checking for ranges of real-valued parameters - these checks are not always true
# if (x_min < 1e-8) | (x_max > 1):
# 	print("x is outside the range! Requires 1e-8 < x < 1")
# if (q_min < 1) | (q_max > 100000):
# 	print("Q is outside the range! Requires 1 < Q < 100000")

# Define the range of values for x and Q
x_range = np.logspace(np.log10(x_min), np.log10(x_max), 1000) # logirthimic sampling
Q_range = np.geomspace(q_min, q_max, 100) # exponential sampling

# Test that the PDF set behaves as expected : All PDFs should be equal to zero at x=1
testFailed=False
for Q in Q_range:
    for id in range(-6, 7):
        if pdf.xfxQ(id, 1, Q)!=0.0:
            print("PDF isn't zero at x=1 for id: "+ str(id) + " Q: " +str(Q)+ " !")
            testFailed=True
            break
    if testFailed:
        isStable = False
        break
            	
# Verify that the PDF set is stable : A PDF can be considered stable if it produces consistent results over multiple evaluations for a given set of parton ID, x, and Q values. 
# This means that if we evaluate the PDF at the same set of parton ID, x, and Q values multiple times, we should get the same or very similar results each time. 
# Taking a random small value of tolerance 1e-6 to verify the same. 
# Compares between different CI invocations now
previousRun = {} # 2D map with map[x][Q] = xfxQ()
previousRunFilePath = "previousRun.pkl"
dataLoaded = os.path.isfile(previousRunFilePath)

if dataLoaded:
    with open(previousRunFilePath, 'rb') as f:
        previousRun = pickle.load(f)

thisRun = {}

for id in (previousRun if len(previousRun)!=0 else range(-6, 7)):
	if id in previousRun and len(previousRun[id])!=0 :
		xRange = previousRun[id]
	else:
		xRange = x_range
	for x in xRange:
		if id in previousRun and x in previousRun[id] and len(previousRun[id][x])!=0:
			QRange = previousRun[id][x]
		else:
			QRange = Q_range
		for Q in QRange:
			if dataLoaded:
				previousResult = previousRun[id][x][Q]
			currentResult = pdf.xfxQ(id, x, Q)
			if id not in thisRun:
				thisRun[id]=dict()
			if x not in thisRun[id]:
				thisRun[id][x]=dict()
			thisRun[id][x][Q]=currentResult
			if dataLoaded and abs(currentResult - previousResult) > 1e-6:
				print("Not a stable PDF")
				isStable = False

if isStable:
    with open(previousRunFilePath, 'wb') as f:
        pickle.dump(thisRun, f)

# partial passing / total passing
if not isStable:
    sys.exit(99)
else:
    sys.exit(0)
